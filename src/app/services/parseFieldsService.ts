import {Injectable} from '@angular/core';
import {FieldBase} from '../interface/parseFields/field-base';
import {IFileds} from '../interface/ilist';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Injectable()
export class ParseFieldsService {

    public fields: FieldBase<any>[]

    public listFields: IFileds[]


    constructor() {
        this.fields = []
        this.listFields = [
            {
                index: 1,
                label: 'Nombre',
                type: 'TEXT',
                required: true,
                search: true,
                order: 1,
                status: true,
                subcategories: []
            },
            {
                index: 2,
                label: 'Edad',
                type: 'NUM',
                required: true,
                search: true,
                order: 2,
                status: true,
                subcategories: []
            },
            {
                index: 3,
                label: 'Sexo',
                type: 'SCAT',
                required: true,
                search: true,
                order: 3,
                status: true,
                subcategories: [
                    {
                        label: 'Masculino'
                    },
                    {
                        label: 'Femenino'
                    }
                ]
            }
        ]
    }

    public setFieldsForm(listFields: IFileds[]) {
        this.fields = []
        listFields.forEach( listField => {
            this.fields = [
                ...this.fields,
                {
                  name: listField.label.toLowerCase(),
                  index: listField.index,
                  label: listField.label,
                  type: listField.type,
                  required: listField.required,
                  search: listField.search,
                  order: listField.order,
                  status: listField.status,
                  subcategories: listField.subcategories
                }]
        })
        return this.fields.sort((a, b) => {
            return a.order - b.order
        })
    }

    public toFormGroup(questions: FieldBase<any>[]) {
        const group: any = {};
        questions.forEach(question => {
            const validations: any[] = [];
            if (question.required === true) {
                validations.push(Validators.required)
            }
            group[question.index] = new FormControl( '', validations)
        });
        return new FormGroup(group)
    }
}
