import {Injectable} from '@angular/core';
import {Router} from '@angular/router';


@Injectable()
export class AuthGuardService {
    constructor(private router: Router) {
    }

    canActivate() {
        const token = localStorage.getItem('token')
        if (token) {
            return true;
        }
        this.router.navigate(['/login'])
        return false;
    }
}
