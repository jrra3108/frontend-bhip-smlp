import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {finalize} from 'rxjs/operators';
import {IList} from '../interface/ilist';
import {IListaCatalog} from '../interface/catalogs/lista-catalog';
import {IUsuarioCatalog} from '../interface/catalogs/usuario-catalog';
import {IRegisterList} from '../interface/iRegisterList';
import {IRoleCatalog} from '../interface/catalogs/role-catalog';


@Injectable()
export class ApiService {

    private url = environment.apiUrl;

    constructor(private http: HttpClient,
                private spinner: NgxSpinnerService) {
    }

    public hideSpinner() {
        setTimeout(() =>
                this.spinner.hide(),
            1000);
    }

    // Estructuras de Listas

    public createList(body): Observable<IList> {
        this.spinner.show()
        return this.http.post<IList>(`${this.url}/listaspreven`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getList(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/listaspreven`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListById(id: number): Observable<IList> {
        this.spinner.show()
        return this.http.get<IList>(`${this.url}/listaspreven/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListFilter(idList, idUser) {
        this.spinner.show();
        return this.http.get<any>(`${this.url}/listaspreven/filter/${idList}/${idUser}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }
    // Estructuras de Listas

    // Otros

    public registerProfile(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/profiles`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getProfileById(id: number): Observable<any > {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/profiles/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public registerArea(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/areas`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getAllArea(): Observable<any > {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/areas`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public regUserArea(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/areas/resp`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getUserAreaById(id: number): Observable<any > {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/areas/resp/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateArea(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/areas/update`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public removeUserArea(id: number): Observable<any > {
        this.spinner.show()
        return this.http.delete<any>(`${this.url}/areas/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getAllListConex() {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/conexlist`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public registerConexList(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/conexlist`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateConexList(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/conexlist/update`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public uploadReg(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/upload`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getUltimUp(id): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/upload/ultimate/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // Otros

    // Registros en Listas

    public registerPerson(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/lista/registro`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updatePerson(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/lista/registro/update`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRegisterList(id: number): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.get<IRegisterList[]>(`${this.url}/lista/registro/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRegisterById(id: number): Observable<IRegisterList> {
        this.spinner.show()
        return this.http.get<IRegisterList>(`${this.url}/lista/registro/find/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public filterAllList(body): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.post<IRegisterList[]>(`${this.url}/lista/registro/filter`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public filterByList(body): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.post<IRegisterList[]>(`${this.url}/lista/registro/filter/list`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }
    // Registros en Listas

    // request catalogs

    public getListCatalog(): Observable<IListaCatalog[]> {
        this.spinner.show()
        return this.http.get<IListaCatalog[]>(`${this.url}/listaspreven/catalog`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getUserCatalog(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/usuarios`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRolesList(): Observable<IRoleCatalog> {
        this.spinner.show();
        return this.http.get<IRoleCatalog>(`${this.url}/usuarios/roles`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }
    // request catalogs
}
