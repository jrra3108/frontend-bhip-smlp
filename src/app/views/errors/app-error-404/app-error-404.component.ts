import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';


@Component({
    selector: 'app-error-404',
    templateUrl: './app-error-404.component.html'
})
export class AppError404Component implements OnInit {
    constructor(private router: Router) {
    }

    ngOnInit(): void {
    }

    public return() {
        this.router.navigate(['/app/home']);
    }
}
