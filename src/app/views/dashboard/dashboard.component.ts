import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {

  public profile;
  public role;

  constructor( ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    this.role = JSON.parse(localStorage.getItem('role'));

    console.log(this.profile);
    console.log(this.role);
  }

}
