import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AuthService} from '../../../services/authService';
import {Usuario} from '../usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup

  constructor(
      private fb: FormBuilder,
      private router: Router,
      public toastr: ToastrManager,
      private auth: AuthService
  ) { }

  ngOnInit() {
    this.builForm();
  }

  private builForm() {
    this.loginForm = this.fb.group( {
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  login() {
    if (this.loginForm.valid) {

      const user: Usuario = {
        username: this.loginForm.getRawValue().username,
        password: this.loginForm.getRawValue().password
      }
      // this.router.navigate(['/app/home']);
      // this.toastr.successToastr('Bienvenido al sistema', 'Inicio de Sesión');
      this.auth.login(user).subscribe(  resp => {
        console.log(resp)
          if (resp.response.code === '00') {
              localStorage.setItem('token', resp.token);
              this.auth.getRoles().subscribe( profile => {
                  localStorage.setItem('profile', JSON.stringify(profile.profile));
                  this.router.navigate(['/login/roles'])
              });
              // this.router.navigate(['/app/home']);
              // this.toastr.successToastr('Bienvenido al sistema', 'Inicio de Sesión');
          } else if (resp.response.code === '01') {
              this.toastr.warningToastr('Usuario/Clave incorrectos', 'Inicio de Sesión');
          } else {
              this.toastr.errorToastr('Error de comuniciación', 'Inicio de Sesión');
          }
      }, error1 => {
        console.log(error1)
        console.log(error1.status)
        if ( error1.status === 400 ) {
          this.toastr.warningToastr('Credenciales incorrctas', 'Inicio de Sesión');
        } else {
          this.toastr.errorToastr('Error de comuniciación', 'Inicio de Sesión');
        }
      })

    } else {
      this.toastr.warningToastr('Ingrese usuario y contraseña correctamente', 'Inicio de Sesión');
    }
  }

}
