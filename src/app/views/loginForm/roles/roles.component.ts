import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

    private profile;
    private roles = [];

    constructor(
        private router: Router
    ) {
        this.profile = JSON.parse(localStorage.getItem('profile'));
        console.log(this.profile);
        this.roles = this.profile.roles;
    }

    ngOnInit() {
        if (this.roles.length === 1) {
            localStorage.setItem('role', JSON.stringify(this.roles[0]));
            this.router.navigate(['/app/home']);
        }
    }

    public selectRole(rol) {
        localStorage.setItem('role', JSON.stringify(rol));
        this.router.navigate(['/app/home']);
    }
}
