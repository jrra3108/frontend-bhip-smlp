import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mantto-individual-edit',
  templateUrl: './mantto-individual-edit.component.html',
  styleUrls: ['./mantto-individual-edit.component.scss']
})
export class ManttoIndividualEditComponent implements OnInit {

  public TITLE = 'Actualización de Información de Persona'

  constructor(
      private router: Router
  ) { }

  ngOnInit() {
  }

  public cancel() {
    this.router.navigate(['/app/upload/mantto-individual/list'])
  }

}
