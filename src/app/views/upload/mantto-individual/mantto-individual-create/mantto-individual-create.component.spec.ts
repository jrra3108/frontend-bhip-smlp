import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoIndividualCreateComponent } from './mantto-individual-create.component';

describe('ManttoIndividualCreateComponent', () => {
  let component: ManttoIndividualCreateComponent;
  let fixture: ComponentFixture<ManttoIndividualCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoIndividualCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoIndividualCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
