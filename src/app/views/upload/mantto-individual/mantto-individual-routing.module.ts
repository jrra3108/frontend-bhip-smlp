import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ManttoIndividualListComponent} from './mantto-individual-list/mantto-individual-list.component';
import {ManttoIndividualEditComponent} from './mantto-individual-edit/mantto-individual-edit.component';
import {ManttoIndividualCreateComponent} from './mantto-individual-create/mantto-individual-create.component';
import {ManttoIndividualLoadComponent} from './mantto-individual-load/mantto-individual-load.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ManttoIndividualListComponent,
        data: {
            title: 'Mantenimiento Individual de Listas - Listado'
        }
    },
    {
        path: 'edit',
        component: ManttoIndividualEditComponent,
        data: {
            title: 'Mantenimiento Individual de Listas - Editar Persona '
        }
    },
    {
        path: 'create',
        component: ManttoIndividualCreateComponent,
        data: {
            title: 'Mantenimiento Individual de Listas - Crear Persona '
        }
    },
    {
        path: 'load',
        component: ManttoIndividualLoadComponent,
        data: {
            title: 'Mantenimiento Individual de Listas - Carga '
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ManttoIndividualRoutingModule {
}
