import {NgModule} from '@angular/core';
import {ManttoIndividualRoutingModule} from './mantto-individual-routing.module';
import {ManttoIndividualListComponent} from './mantto-individual-list/mantto-individual-list.component';
import { ManttoIndividualEditComponent } from './mantto-individual-edit/mantto-individual-edit.component';
import { ManttoIndividualCreateComponent } from './mantto-individual-create/mantto-individual-create.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ManttoIndividualLoadComponent} from './mantto-individual-load/mantto-individual-load.component';


@NgModule({
    imports: [
        ManttoIndividualRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        ManttoIndividualListComponent,
        ManttoIndividualEditComponent,
        ManttoIndividualCreateComponent,
        ManttoIndividualLoadComponent]
})

export class ManttoIndividualModule {
}
