import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoIndividualLoadComponent } from './mantto-individual-load.component';

describe('ManttoIndividualLoadComponent', () => {
  let component: ManttoIndividualLoadComponent;
  let fixture: ComponentFixture<ManttoIndividualLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoIndividualLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoIndividualLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
