import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../services/apiService';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {IList} from '../../../../interface/ilist';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IRegisterList, IRegisterPerson} from '../../../../interface/iRegisterList';

@Component({
  selector: 'app-mantto-individual-load',
  templateUrl: './mantto-individual-load.component.html',
  styleUrls: ['./mantto-individual-load.component.scss']
})
export class ManttoIndividualLoadComponent implements OnInit {

  public TITLE = 'Carga Individual'
  public listaCatalog: IListaCatalog[]
  public valueIdList: number

  public listaPP: IList
  public createStatus = true

  public fieldsRegister: IRegisterList[]

  constructor(
      private apiService: ApiService,
      private router: Router,
      private toastr: ToastrManager
  ) { }

  ngOnInit() {
    this.getListCatalog()
  }

  public getListPP() {
    console.log(this.valueIdList)
    if (this.valueIdList !== undefined) {
      this.apiService.getListById(this.valueIdList).subscribe(data => {
        this.toastr.successToastr('Información de la lista se ha cargado con éxito', this.TITLE);
        this.listaPP = data
        this.getRegList()
        this.createStatus = false
      })
    } else {
      this.toastr.warningToastr('Debe seleccionar una lista', this.TITLE);
    }
  }

  public getRegList() {
    this.apiService.getRegisterList(this.valueIdList).subscribe( data => {

      this.fieldsRegister = data
      // console.log(this.fieldsRegister)
    })
  }

  private getListCatalog() {
    this.apiService.getListCatalog().subscribe( data => {
      this.listaCatalog = data
    })
  }

  public createPerson() {
    this.router.navigate(['/app/upload/mantto-individual/create', {create: true, idList: this.valueIdList}])
  }

  public onChange() {
    this.createStatus = true
  }

  public cancel() {
    this.router.navigate(['/app/upload/mantto-individual/list'])
  }

  private orderList(list) {
    console.log(list)
    return list.sort((a, b) => {
      return a.order - b.order
    })
  }

}
