import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoIndividualListComponent } from './mantto-individual-list.component';

describe('ManttoIndividualListComponent', () => {
  let component: ManttoIndividualListComponent;
  let fixture: ComponentFixture<ManttoIndividualListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoIndividualListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoIndividualListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
