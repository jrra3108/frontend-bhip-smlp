import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {IFileds, IList} from '../../../../interface/ilist';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {ParseFieldsService} from '../../../../services/parseFieldsService';
import {FormGroup} from '@angular/forms';
import {IRegisterList} from '../../../../interface/iRegisterList';

@Component({
  selector: 'app-mantto-individual-list',
  templateUrl: './mantto-individual-list.component.html',
  styleUrls: ['./mantto-individual-list.component.scss']
})
export class ManttoIndividualListComponent implements OnInit {

  public TITLE = 'Mantenimiento individual de Listas'
  public modalRef: BsModalRef

  public listaList: IListaCatalog[]
  public valueList: number
  public fields: FieldBase<any>[]
  public listSearchForm: FormGroup

  public fieldsRegister: IRegisterList[]
  public listaPP: IList
  public fNameAll;
  public fNacAll;
  public fFechaAll;
  public fTdocAll;
  public fDocAll;

  constructor(
      private router: Router,
      private modalService: BsModalService,
      private apiService: ApiService,
      private parse: ParseFieldsService
  ) {
  }

  ngOnInit() {
    this.fields = []
    this.getList()
  }

  private getList() {
    this.apiService.getListCatalog().subscribe( data => {
      this.listaList = data;
    })
  }

  public filterAllList() {

    const filters = [];

    if ( this.fNameAll !== undefined && this.fNameAll !== null && this.fNameAll.trim() !== '') {
      filters.push({
        index: 100201901,
        type: 'TEXT',
        value: this.fNameAll,
      })
    }

    if ( this.fNacAll !== undefined && this.fNacAll !== null && this.fNacAll.trim() !== '') {
      filters.push({
        index: 100201902,
        type: 'TEXT',
        value: this.fNacAll,
      })
    }

    if ( this.fFechaAll !== undefined && this.fFechaAll !== null && this.fFechaAll.trim() !== '') {
      filters.push({
        index: 100201903,
        type: 'DATE',
        value: this.fFechaAll,
      })
    }

    if ( this.fTdocAll !== undefined && this.fTdocAll !== null && this.fTdocAll.trim() !== '') {
      filters.push({
        index: 100201904,
        type: 'TEXT',
        value: this.fTdocAll,
      })
    }

    if ( this.fDocAll !== undefined && this.fDocAll !== null) {
      filters.push({
        index: 100201905,
        type: 'NUM',
        value: this.fDocAll,
      })
    }

    this.apiService.filterAllList(filters).subscribe( data => {
        console.log(data);
        this.fieldsRegister = data;
      this.fieldsRegister.forEach( line => {
          const regs = []
          let i = 0;
          line.registro.forEach( reg => {
            if (i < 5) {
              regs.push(reg)
            }
            i++
          })
          line.registro = regs;
          console.log(line.registro)
        })
    })
  }

  public createPeople() {
    this.router.navigate(['/app/upload/mantto-individual/load'])
  }
  public editPeople() {
    this.router.navigate(['/app/upload/mantto-individual/edit'])
  }

  public modalAnulation(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
  }

  public onChange() {

    if (this.valueList > 0) {
      this.apiService.getListById(this.valueList).subscribe( data => {
        // console.log(data)
        // console.log(this.fieldSearch(data.data))
        this.listaPP = data
        this.fields = this.parse.setFieldsForm(this.fieldSearch(data.data))
        console.log(this.fields)
        this.listSearchForm = this.parse.toFormGroup(this.fields)
        console.log(this.listSearchForm)
        this.apiService.getRegisterList(this.valueList).subscribe( regData => {
            this.fieldsRegister = regData
        })
      })
    }
  }

  public fieldSearch(data) {
    const fl: IFileds[] =  []
    data.estructura.fields.forEach( field => {
      if (field.search === true) {
        fl.push(field)
      }
    })
    return fl
  }

  private orderList(list) {
    return list.sort((a, b) => {
      return a.order - b.order
    })
  }

  public editar(id, reg) {
    this.router.navigate(['/app/upload/mantto-individual/create', {create: false, idList: id, reg: reg}])
  }

  public filterIdList() {
    const filters = [];
    const listFormKeys = Object.keys(this.listSearchForm.controls);
    listFormKeys.forEach( fieldForm => {
      const field = this.fields.filter( item => item.index === Number(fieldForm))[0];

      if ( this.listSearchForm.controls[fieldForm].value !== undefined &&
          this.listSearchForm.controls[fieldForm].value !== null &&
          this.listSearchForm.controls[fieldForm].value.trim() !== '') {
        if (field.type === 'TEXT' || field.type === 'DATE') {
          filters.push({
            index: field.index,
            type: field.type,
            value: this.listSearchForm.controls[fieldForm].value
          })
        }

        if (field.type === 'NUM') {
          filters.push({
            index: field.index,
            type: field.type,
            value: Number(this.listSearchForm.controls[fieldForm].value)
          })
        }

        if (field.type === 'SCAT') {
          filters.push({
            index: field.index,
            type: 'TEXT',
            value: this.listSearchForm.controls[fieldForm].value
          })
        }
      }
    })

    const params = {
      idList: this.valueList,
      data: filters
    }
    this.apiService.filterByList(params).subscribe( data => {
      console.log(data);
      this.fieldsRegister = data;
    })
  }

}
