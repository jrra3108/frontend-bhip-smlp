import {NgModule} from '@angular/core';
import {CargaMasivaListComponent} from './carga-masiva-list/carga-masiva-list.component';
import {CargaMasivaRoutingModule} from './carga-masiva-routing.module';
import { ConsultaCargasComponent } from './consulta-cargas/consulta-cargas.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    imports: [
        CargaMasivaRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [CargaMasivaListComponent, ConsultaCargasComponent]
})

export class CargaMasivaModule {
}
