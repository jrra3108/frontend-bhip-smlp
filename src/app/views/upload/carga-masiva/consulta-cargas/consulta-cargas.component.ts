import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-consulta-cargas',
  templateUrl: './consulta-cargas.component.html',
  styleUrls: ['./consulta-cargas.component.scss']
})
export class ConsultaCargasComponent implements OnInit {

  public TITLE = 'Consulta de Cargas'
  public modalRef: BsModalRef

  constructor(
      private router: Router,
      private modalService: BsModalService
  ) { }

  ngOnInit() {
  }

  public returnPage() {
    this.router.navigate(['/app/upload/carga-masiva/list'])
  }

  public modalAnulation(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
  }

}
