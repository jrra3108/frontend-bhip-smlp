import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaCargasComponent } from './consulta-cargas.component';

describe('ConsultaCargasComponent', () => {
  let component: ConsultaCargasComponent;
  let fixture: ComponentFixture<ConsultaCargasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaCargasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaCargasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
