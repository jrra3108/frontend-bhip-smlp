import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import * as XLSX from 'xlsx';
import {IFileds, IList} from '../../../../interface/ilist';
import {ApiService} from '../../../../services/apiService';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {IRegisterList, IRegisterPerson} from '../../../../interface/iRegisterList';
import {ToastrManager} from 'ng6-toastr-notifications';
type AOA = any[][];

@Component({
  selector: 'app-carga-masiva-list',
  templateUrl: './carga-masiva-list.component.html',
  styleUrls: ['./carga-masiva-list.component.scss']
})
export class CargaMasivaListComponent implements OnInit {

  public TITLE = 'Carga Masiva de Listas'
  public modalRef: BsModalRef
  public valueList: number
  public listaPP: IList
  public headFields: IFileds[]
  public listaList: IListaCatalog[]
  public selectStatus = false
  public listAreas = [];
  public profile;
  public selectArea;
  public valueArea;
  public listRegUpload = [];
  public listregs = [];

  public solicitadoPor = 'Contabilidad';
  public cargadoPor
  public fechaCreacion
  public cantRegs;
  data: AOA;

  arrayBuffer: any;
  file

  constructor(
      private modalService: BsModalService,
      private apiService: ApiService,
      private router: Router,
      public toastr: ToastrManager,
  ) {
      this.profile = JSON.parse(localStorage.getItem('profile'))
      // console.log(this.profile)
  }

  ngOnInit() {
    this.getList()
    this.getAreas()
  }

  public getAreas() {
    this.apiService.getAllArea().subscribe( data => {
      // console.log(data)
      this.listAreas = data;
    })
  }

  private getList() {
    this.apiService.getListCatalog().subscribe( data => {
      this.listaList = data;
      // console.log(this.listaList)
    })
  }

  public onChange() {
    this.selectStatus = false;
    // if (this.valueList > 0) {
    // }
  }

  public selectedList () {
    this.selectStatus = true
    this.apiService.getListById(this.valueList).subscribe(data => {
      // console.log(data)
      // console.log(this.fieldSearch(data.data))
      this.toastr.successToastr('Información de Lista cargada con éxito', this.TITLE)
      this.listaPP = data
      this.headFields = this.listaPP.data.estructura.fields
    })
    this.apiService.getUltimUp(this.valueList).subscribe( upList => {
      console.log(upList);
      this.listRegUpload = upList
      this.fechaCreacion = this.listRegUpload[0].upList_fechaCarga;
      this.cantRegs = this.listRegUpload[0].upList_CantReg;
      if (this.listRegUpload.length === 0) {
          this.toastr.warningToastr('No se ha realizado carga de archivos', this.TITLE)
      }
    })
  }

  public onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  public validate(template) {
    this.selectArea = {
        id: this.valueArea,
        name: this.findName(this.valueArea)
    }
    this.closeModal();
    const newData = []
    if ( this.data !== undefined) {
      this.modalRef = this.modalService.show(template, {
        class: 'modal-primary modal-lg',
        backdrop: true,
        ignoreBackdropClick: true,
        keyboard: false
      })
    }
  }

  public importData() {

    const importList = []

    this.data.forEach( lineData => {
      const linesRegister = []
      let i = 0;
      this.headFields.forEach( head => {
          linesRegister.push({
            idField: head.index,
            order: head.order,
            value: lineData[i]
          })
        i++;
      })
      // console.log(linesRegister)
      const register: IRegisterList = {
        codLista: this.valueList,
        registro: linesRegister,
        masivoIndivudual: 'M',
        estado: true,
        observaciones: '',
        idCarga: 0
      }
      importList.push(register)
    })

    const body = {
      idLista: this.valueList,
      idArea: this.valueArea,
      idUser: this.profile.idUsuario,
      CantReg: this.data.length,
      registros: importList,
    }
    console.log(body)
    this.apiService.uploadReg(body).subscribe( resp => {
      if (resp.code === '00') {
        this.modalRef.hide();
        this.toastr.successToastr('Archivo Cargado con exito', this.TITLE)
        this.apiService.getUltimUp(this.valueList).subscribe( upList => {
          console.log(upList);
          this.listRegUpload = upList
          this.fechaCreacion = this.listRegUpload[0].upList_fechaCarga;
          this.cantRegs = this.listRegUpload[0].upList_CantReg;
          if (this.listRegUpload.length === 0) {
            this.toastr.warningToastr('No se ha realizado carga de archivos', this.TITLE)
          }
        })
      } else {
        this.toastr.errorToastr('No fue posible cargar el archivo', this.TITLE)
      }
    })
  }

  public closeModal() {
    this.modalRef.hide()
  }

  public upFileList(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public goConsult() {
    this.router.navigate(['/app/upload/carga-masiva/consulta'])
  }

    public findName(id) {
        return this.listAreas.filter(item => Number(item.id) === Number(id))[0].nombre
    }
}
