import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaMasivaListComponent } from './carga-masiva-list.component';

describe('CargaMasivaListComponent', () => {
  let component: CargaMasivaListComponent;
  let fixture: ComponentFixture<CargaMasivaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargaMasivaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaMasivaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
