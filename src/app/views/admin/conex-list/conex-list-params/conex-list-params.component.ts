import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {ApiService} from '../../../../services/apiService';

@Component({
  selector: 'app-conex-list-params',
  templateUrl: './conex-list-params.component.html',
  styleUrls: ['./conex-list-params.component.scss']
})
export class ConexListParamsComponent implements OnInit {

  public TITLE = 'Conexion a Listas Externas';
    public modalRef: BsModalRef;
    public valueList
    public listCatalog: IListaCatalog[]
    public valueUrl: string
    public conexList = [];
    public  idValueEdit: number
    public urlValueEdit: string;
  constructor(private modalService: BsModalService,
              private apiService: ApiService
  ) { }

  //  conexList_id: 3
  //  conexList_urlConexion: "Prueba"
  //  lista_idLista: "Lista02"

  ngOnInit() {
      this.apiService.getListCatalog().subscribe( data => {
          this.listCatalog = data
      })
      this.getAllConexList()
  }

  public getAllConexList() {
     this.apiService.getAllListConex().subscribe( data => {
         console.log(data)
         this.conexList = data
     })
  }

  public openModal(template) {
      this.valueUrl = ''
      this.modalRef = this.modalService.show(template, {
          class: 'modal-lg modal-primary',
          backdrop: true,
          ignoreBackdropClick: true,
          keyboard: false
      })
  }

    public openEditModal(template, id, idLista, urlConex) {
        this.idValueEdit = id;
        this.valueList = idLista;
        this.urlValueEdit = urlConex;
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public EditConex() {
      const body = {
          id: this.idValueEdit,
          idLista: this.valueList,
          urlConexion: this.urlValueEdit
      }

      this.apiService.updateConexList(body).subscribe( data => {
          this.modalRef.hide();
          this.getAllConexList()
      })
    }

  public createConexList() {
     const body = {
         idLista: this.valueList,
         urlConexion: this.valueUrl
     }

     this.apiService.registerConexList(body).subscribe( data => {
         console.log(data);
         this.modalRef.hide();
         this.getAllConexList()
     })
  }

  public closeModal() {
      this.modalRef.hide();
  }

}
