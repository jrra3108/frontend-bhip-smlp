import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ApiService} from '../../../../services/apiService';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IRoleCatalog} from '../../../../interface/catalogs/role-catalog';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IFileds, IList} from '../../../../interface/ilist';
import {IFieldsProfile, IProfile} from '../../../../interface/iprofile';

@Component({
  selector: 'app-asig-perfiles-list',
  templateUrl: './asig-perfiles-list.component.html',
  styleUrls: ['./asig-perfiles-list.component.scss']
})
export class AsigPerfilesListComponent implements OnInit {

  public TITLE = 'Asignación de Perfiles'

  public modalRef: BsModalRef
  public listCatalog: IListaCatalog[]
  public addProfileForm: FormGroup
  public listRoles: IRoleCatalog
  public valueList
  public createStatus = true
  public listaPP: IList
  public fieldsProfile: IFieldsProfile[]
  public isChecked
  public profile: Array<IProfile>  = new Array<IProfile>()
  public fieldsProf = [];

  constructor(
      private modalService: BsModalService,
      public router: Router,
      private apiService: ApiService,
      private fb: FormBuilder,
      private toastr: ToastrManager
  ) { }

  ngOnInit() {
    this.buildForm();
    this.apiService.getListCatalog().subscribe( data => {
      this.listCatalog = data
    })

    this.apiService.getRolesList().subscribe( data => {
      this.listRoles = data;
      // console.log(data.data);
    })
  }

  public selectList() {
    console.log(this.valueList)
    if (this.valueList !== undefined) {
      this.getById();
      this.apiService.getListById(this.valueList).subscribe(data => {
        this.toastr.successToastr('Información de la lista se ha cargado con éxito', this.TITLE);
        this.listaPP = data
        this.listaPP.data.estructura.fields.forEach( field => {
          this.fieldsProf.push({
            index: field.index,
            label: field.label,
            value: false,
          })
        })
        this.createStatus = false
      })
    } else {
      this.toastr.warningToastr('Debe seleccionar una lista', this.TITLE);
    }
  }

  public setValueField() {
    // console.log(index)
  }

  private buildForm() {
    this.addProfileForm = this.fb.group({
      profileField: ['', Validators.compose([Validators.required])],
      accessField: ['', Validators.compose([Validators.required])],
      viewField: ['', Validators.compose([Validators.required])],
      downloadField: ['', Validators.compose([Validators.required])],
      managerField: ['', Validators.compose([Validators.required])],
    })
  }

  public editFieldsList(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
    this.fieldsProf = []
  }

  public cancel() {
    this.router.navigate(['/app/home'])
  }

  public openModal(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public addProfileToList () {
    const profile: IProfile = {
      profile_rol: Number(this.addProfileForm.getRawValue().profileField),
      profile_acceso: this.addProfileForm.getRawValue().accessField,
      profile_visualizacion: this.addProfileForm.getRawValue().viewField,
      profile_descarga: this.stringToBool(this.addProfileForm.getRawValue().downloadField),
      profile_responsable: this.stringToBool(this.addProfileForm.getRawValue().managerField),
      fields: this.fieldsProfile,
      profile_idLista: Number(this.valueList)
    }
    console.log(profile);
    // this.profile.push(profile)
    this.modalRef.hide()
    this.fieldsProf = []
    this.apiService.registerProfile(profile).subscribe( data => {
      // console.log(data);
      this.getById()
      this.addProfileForm.reset();
    })
  }

  public onChange() {
    this.createStatus = true
  }

  public getById() {
    this.apiService.getProfileById(this.valueList).subscribe( data => {
      this.profile = data;
      console.log(this.profile)
    })
  }

  public changeCheckBox(id) {
    console.log(id);
  }

  private stringToBool(value): boolean {

    switch (value) {
      case 'true':
        return true
      case true:
        return true
      case 'false':
        return false
      case false:
        return false
    }
  }

}
