import { RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


export const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'mantto-listas',
                loadChildren: './mantto-listas/mantto-listas.module#ManttoListasModule',
            },
            {
                path: 'asig-perfiles',
                loadChildren: './asig-perfiles/asig-perfiles.module#AsigPerfilesModule',
            },
            {
                path: 'mantto-areas',
                loadChildren: './mantto-areas/mantto-areas.module#ManttoAreasModule',
            },
            {
                path: 'conex-list',
                loadChildren: './conex-list/conex-list.module#ConexListModule',
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
