import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {ApiService} from '../../../../services/apiService';
import {forEach} from '@angular/router/src/utils/collection';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {IUsuarioCatalog} from '../../../../interface/catalogs/usuario-catalog';
import {IManager} from '../../../../interface/imanager';
import {IFileds, INivCrit} from '../../../../interface/ilist';

@Component({
  selector: 'app-mantto-listas-list',
  templateUrl: './mantto-listas-list.component.html',
  styleUrls: ['./mantto-listas-list.component.scss']
})
export class ManttoListasListComponent implements OnInit {

  public TITLE = 'Mantenimiento de Listas'
  subscriptions: Subscription[] = []
  public modalRef: BsModalRef
  public confirmText: string
  public confirmAction = {
    selectedId: 0,
    action: ''
  }
  // public companies: ICompaniesList
  public instSelected: number
  public pageSelected: number

  // vaiables listas catalogos
  public listCatalog: IListaCatalog[]
  public respCatalog: IUsuarioCatalog[]

  // variables de busqueda
  public valueList
  public valueResp
  public valueDate

  public listTable: any

  constructor(
      // private modalService: BsModalService,
              private changeDetection: ChangeDetectorRef,
              private router: Router,
              private apiService: ApiService
            ) {}

  ngOnInit() {
    this.pageSelected = 0
    this.findDefoult(0, 0);

    this.apiService.getListCatalog().subscribe( data => {
      this.listCatalog = data
      // console.log(this.listCatalog)
    })

    this.apiService.getUserCatalog().subscribe( data => {
      this.respCatalog = data.data
      // console.log(this.respCatalog)
    })
  }

  private findDefoult(idList, idUser) {
    this.apiService.getListFilter(idList, idUser).subscribe( lista => {
      this.listTable = lista.data
      console.log(lista.data)
    })
  }

  public buscarLista() {
    console.log(this.valueList)
    console.log(this.valueResp)
    console.log(this.valueDate)
    if (this.valueList === undefined && this.valueResp === undefined) {
        this.findDefoult(0, 0);
    } else if (this.valueList !== undefined && this.valueResp === undefined) {
        this.findDefoult(this.valueList, 0);
    } else if (this.valueList === undefined && this.valueResp !== undefined) {
        this.findDefoult(0, this.valueResp);
    } else if (this.valueList !== undefined && this.valueResp !== undefined) {
        this.findDefoult(this.valueList, this.valueResp);
    }
  }

  onInstSelected(inst: number) {
    this.instSelected = inst
  }

  onPageSelected(id) {
    this.pageSelected = id
    // const httpParams = new HttpParams()
    //     .set('page', String(this.pageSelected))
    //     .set('size', '8')
    // this.http.getCompaniesList(this.instSelected, httpParams).subscribe( data => {
    //   if (data.response.code === 0) {
    //     this.companies = data
    //   } else {
    //     this.toastr.warning('No se encontraron resultados', 'Comercios')
    //   }
    // })
  }

  public consultar() {

    // const httpParams = new HttpParams()
    //     .set('page', String(this.pageSelected))
    //     .set('size', '8')
    // this.http.getCompaniesList(this.instSelected, httpParams).subscribe( data => {
    //   if (data.response.code === 0) {
    //     this.companies = data
    //   } else {
    //     this.toastr.warning('No se encontraron resultados', 'Comercios')
    //   }
    // })
  }

  onOpenConfirm(action: string, id: number) {
    switch (action) {
      case 'delete':
        this.confirmText = 'Confirme que desea eliminar el Comercio.'
        break
      case 'disable':
        this.confirmText = 'Confirme que desea desactivar la categoria'
        break
    }

    this.confirmAction = {
      selectedId: id,
      action: action
    }

    // const _combine = combineLatest(
    //     this.modalService.onHidden
    // ).subscribe(() => this.changeDetection.markForCheck())
    //
    // this.subscriptions.push(
    //     this.modalService.onHidden.subscribe((reason: string) => {
    //       console.log('Cerrando el modal')
    //       this.onActionUserSelectedReset()
    //       this.unsubscribe()
    //     })
    // )

    // this.subscriptions.push(_combine)

    // this.modalRef = this.modalService.show(ConfirmModalComponent, {class: 'modal-primary'})
    // this.modalRef.content.title = this.TITLE
    // this.modalRef.content.message = this.confirmText
    //
    // this.modalRef.content.onParams$.subscribe(params => {
    //   if (params.confirmAccept === true) {
    //     this.onConfirmOk()
    //   } else {
    //     this.onClose()
    //   }
    // })
  }

  onConfirmOk() {
    switch (this.confirmAction.action) {
      case 'delete':
        this.delete(this.confirmAction.selectedId)
        break
      case 'disable':
        this.disable(this.confirmAction.selectedId)
        break
    }
  }

  onClose() {
    this.modalRef.hide()
  }

  onActionUserSelectedReset() {
    this.confirmAction = {
      selectedId: 0,
      action: ''
    }

    console.log('Reseteo: ', this.confirmAction)
  }

  unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe()
    })
    this.subscriptions = []
  }

  public delete(id) {
    this.modalRef.hide()
    // this.http.deleteCommerce(id).subscribe( data => {
    //   if (data.response.code === 0 ) {
    //     this.toastr.success('Comercio eliminado con éxito', 'Comercios')
    //     const httpParams = new HttpParams()
    //         .set('page', String(this.pageSelected))
    //         .set('size', '8')
    //     this.http.getCompaniesList(this.instSelected, httpParams).subscribe(data1 => {
    //       this.companies = data1
    //     })
    //   } else if (data.response.code === 6) {
    //   //  this.toastr.warning('El Comercio no puede ser eliminado ya que posee transacciones', 'Comercios')
    //   } else {
    //   //  this.toastr.warning(data.response.description, 'Comercios')
    //   }
    // })
  }

  public disable(id) {
    this.modalRef.hide()
    // this.http.disableCommerce(id).subscribe( data => {
    //   if (data.response.code === 0) {
    //     this.toastr.success('El Comercio se ha desabilitado con exito', 'Comercios')
    //     const httpParams = new HttpParams()
    //         .set('page', String(this.pageSelected))
    //         .set('size', '8')
    //     this.http.getCompaniesList(this.instSelected, httpParams).subscribe(data1 => {
    //       this.companies = data1
    //     })
    //   } else {
    //     this.toastr.error('El Comercio no se ha desabilitado', 'Comercios')
    //   }
    // })
  }

  public editRecord(userSelectedId) {
    this.router.navigate([ '/administracion/comercios_editar' ], {
      queryParams: {
        unique: userSelectedId
      }
    })
  }

  public usuarioNuevo() {
    this.router.navigate(['/administracion/comercios_nuevo'])
  }

  public banners() {
    this.router.navigate(['/administracion/comercios_logos'])
  }

  public agregar() {
    this.router.navigate(['app/admin/mantto-listas/agregar'])
  }

}
