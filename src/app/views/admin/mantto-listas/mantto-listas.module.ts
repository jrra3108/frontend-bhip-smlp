import {NgModule} from '@angular/core';
import {ManttoListasRoutingModule} from './mantto-listas-routing.module';
import {ManttoListasListComponent} from './mantto-listas-list/mantto-listas-list.component';
import { ManttoListasAgregarComponent } from './mantto-listas-agregar/mantto-listas-agregar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select'

@NgModule({
    imports: [
        CommonModule,
        ManttoListasRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgSelectModule
    ],
    declarations: [
        ManttoListasListComponent,
        ManttoListasAgregarComponent
    ]
})

export class ManttoListasModule {
}
