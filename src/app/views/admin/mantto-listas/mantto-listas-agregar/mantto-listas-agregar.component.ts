import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {IFileds, IList, INivCrit, ISubCat} from '../../../../interface/ilist';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ConfirmComponentComponent} from '../../../shared/components';
import {IManager} from '../../../../interface/imanager';
import {ILabel} from '../../../../interface/iLabel';
import {ApiService} from '../../../../services/apiService';
import {NgOption} from '@ng-select/ng-select'


@Component({
  selector: 'app-mantto-listas-agregar',
  templateUrl: './mantto-listas-agregar.component.html',
  styleUrls: ['./mantto-listas-agregar.component.scss']
})
export class ManttoListasAgregarComponent implements OnInit {

  public TITLE = 'Crear Lista'

  public modalRef: BsModalRef

  public idList: string
  public defineList: IList
  //     = new class implements IList {
  //   idlist: string;
  //   description: string;
  //   status: boolean;
  //   estructura: {
  //     fields: Array<IFileds>;
  //     managers: Array<IManager>;
  //     nivCrits: INivCrit = new class;
  //   }
  // }
  public descList: string
  public statusList: boolean
  public publicList: boolean

  public addFieldForm: FormGroup
  public fields: Array<IFileds>  = new Array<IFileds>()
  public selectedScat = false
  public arrayItemsScat: Array<ISubCat>  = new Array<ISubCat>()
  // public indexField: number
  public indexEdit: number
  public respField: string
  public addManager: Array<IManager> = new Array<IManager>()
  public messageDft: string
  public nivelMsg: string
  public nivelNum: number
  public arrayNivCrit: Array<ILabel> = new Array<ILabel>()

  public respSelected: NgOption;
  public serviceOptions: NgOption[];

  constructor(
      private modalService: BsModalService,
      private router: Router,
      private fb: FormBuilder,
      public toastr: ToastrManager,
      private api: ApiService
  ) {
    // this.indexField = 0
    this.indexEdit = 0
    this.idList = ''
    this.descList = ''
    this.statusList = true
    this.publicList = true

    this.fields.push({
      index: 100201901,
      label: 'Nombre',
      type: 'TEXT',
      required: true,
      search: true,
      order: 1,
      status: true,
      subcategories: this.arrayItemsScat
    })

    this.fields.push({
      index: 100201902,
      label: 'Nacionalidad',
      type: 'TEXT',
      required: true,
      search: true,
      order: 2,
      status: true,
      subcategories: this.arrayItemsScat
    })

    this.fields.push({
      index: 100201903,
      label: 'Fecha nacimiento',
      type: 'DATE',
      required: true,
      search: true,
      order: 3,
      status: true,
      subcategories: this.arrayItemsScat
    })

    this.fields.push({
      index: 100201904,
      label: 'Tipo documento',
      type: 'SCAT',
      required: true,
      search: true,
      order: 4,
      status: true,
      subcategories: [
        { label: 'DUI'},
        { label: 'NIT'}
      ]
    })

    this.fields.push({
      index: 100201905,
      label: 'Documento',
      type: 'NUM',
      required: true,
      search: true,
      order: 5,
      status: true,
      subcategories: this.arrayItemsScat
    })
  }

  ngOnInit() {
    this.addFieldBuildForm()
    this.respList()
  }

  public respList() {
    this.api.getUserCatalog().subscribe( data => {
      this.serviceOptions = data.data.map( resp => {
        return {
          id: resp.id,
          resp: resp.usuario
        }
      })
    })
  }

  public saveList() {
    if (this.idList !== null && this.idList !== undefined && this.idList !== '') {

      if (this.descList !== null && this.descList !== undefined && this.descList !== '') {

        if (this.statusList !== undefined) {

          if (this.publicList !== undefined) {

            if (this.addManager.length > 0) {

              if (this.fields.length > 0) {

                const list: IList = {
                  data: {
                    idlist: this.idList,
                    description: this.descList,
                    status: this.statusList,
                    managers: this.addManager,
                    estructura: {
                      fields: this.fields,
                      nivCrits: {
                        msgDft: this.messageDft,
                        nivMsg: this.arrayNivCrit
                      }
                    },
                    observaciones: '',
                    publica: this.publicList
                  }
                }

                this.api.createList(list).subscribe( data => {
                  if (data.response.code === '00') {
                    this.toastr.successToastr('La estructuta de la lista ha sido creada con éxito', this.TITLE);
                    this.router.navigate(['/app/admin/mantto-listas/list'])
                  }
                })

              } else {
                this.toastr.warningToastr('Debe agregar al menos un campo a la lista', this.TITLE);
              }
            } else {
              this.toastr.warningToastr('Debe agregar al menos un responsable a la lista', this.TITLE);
            }
          } else {
            this.toastr.warningToastr('Debe definir si la lista es pública o no', this.TITLE);
          }
        } else {
          this.toastr.warningToastr('Debe difinir el estado de la lista', this.TITLE);
        }
      } else {
        this.toastr.warningToastr('Debe agregar una descripción a la lista', this.TITLE);
      }
    } else {
      this.toastr.warningToastr('Debe asignar un id a la lista', this.TITLE);
    }
  }

  private addFieldBuildForm() {
    this.selectedScat = false
    this.arrayItemsScat = new Array<ISubCat>()
    this.addFieldForm = this.fb.group({
      nameField: ['', Validators.compose([Validators.required])],
      typeField: ['TEXT', Validators.compose([Validators.required])],
      requiredField: [true, Validators.compose([Validators.required])],
      searchField: [true, Validators.compose([Validators.required])],
      orderField: [1, Validators.compose([Validators.required])],
      statusField: [true, Validators.compose([Validators.required])],
      itemScat: ['']
    })
  }

  public addFieldToList() {

    console.log(this.fields)
    if (this.addFieldForm.valid) {

      if (!this.validateDupField(
          {
            index: Date.now(),
            label: this.addFieldForm.getRawValue().nameField,
            type: this.addFieldForm.getRawValue().typeField,
            required: this.stringToBool(this.addFieldForm.getRawValue().requiredField),
            search: this.stringToBool(this.addFieldForm.getRawValue().searchField),
            order: this.addFieldForm.getRawValue().orderField,
            status: this.stringToBool(this.addFieldForm.getRawValue().statusField),
            subcategories: this.arrayItemsScat
          }
      )) {


      if (this.addFieldForm.getRawValue().typeField === 'SCAT') {

        if (this.arrayItemsScat.length > 0) {
          this.modalRef.hide()
          // this.indexField++
          this.fields.push({
            index: Date.now(),
            label: this.addFieldForm.getRawValue().nameField,
            type: this.addFieldForm.getRawValue().typeField,
            required: this.stringToBool(this.addFieldForm.getRawValue().requiredField),
            search: this.stringToBool(this.addFieldForm.getRawValue().searchField),
            order: this.addFieldForm.getRawValue().orderField,
            status: this.stringToBool(this.addFieldForm.getRawValue().statusField),
            subcategories: this.arrayItemsScat
          })
          this.addFieldBuildForm()
          this.arrayItemsScat = new Array<ISubCat>()
        } else {
          this.toastr.warningToastr('Debe agregar items a la subcategoria', this.TITLE);
        }

      } else {

        this.modalRef.hide()
        // this.indexField++
        this.fields.push({
          index: Date.now(),
          label: this.addFieldForm.getRawValue().nameField,
          type: this.addFieldForm.getRawValue().typeField,
          required: this.stringToBool(this.addFieldForm.getRawValue().requiredField),
          search: this.stringToBool(this.addFieldForm.getRawValue().searchField),
          order: this.addFieldForm.getRawValue().orderField,
          status: this.stringToBool(this.addFieldForm.getRawValue().statusField),
          subcategories: this.arrayItemsScat
        })
        this.addFieldBuildForm()
        this.arrayItemsScat = new Array<ISubCat>()
      }

      } else {
        this.toastr.warningToastr('El campo que desea agregar ya existe', this.TITLE);
      }

    } else {
      this.toastr.warningToastr('Favor completar toda la información solicitada', this.TITLE);
    }
  }

  public addScatToList() {
    if (this.addFieldForm.getRawValue().itemScat !== ''
        && this.addFieldForm.getRawValue().itemScat !== null
        && this.addFieldForm.getRawValue().itemScat !== undefined) {
      this.arrayItemsScat.push({
        label: this.addFieldForm.getRawValue().itemScat
      })
      this.addFieldForm.controls['itemScat'].setValue('')
    } else {
      this.toastr.warningToastr('Ingrese un item para agregar a la lista', this.TITLE);
    }
  }

  public addRepToList() {
    console.log(this.respSelected)
    if (this.respSelected !== null && this.respSelected !== undefined) {
      this.addManager.push({
        id: this.respSelected.id,
        label: this.respSelected.resp
      })
    } else {
      this.toastr.warningToastr('Ingrese un nombre para agregar a la lista', this.TITLE);
    }
  }

  public addMsgToList() {

    if (this.nivelNum > 0 && this.nivelMsg !== null && this.nivelMsg !== '' && this.nivelMsg !== undefined) {
      this.arrayNivCrit.push({
        id: this.nivelNum,
        label: this.nivelMsg
      })
      this.nivelNum = 1
      this.nivelMsg = ''
    } else {
      this.toastr.warningToastr('Ingrese un numero de nivel y un mensaje para agregar', this.TITLE);
    }
  }

  public editToField() {

    if (this.addFieldForm.valid) {

      if (this.addFieldForm.getRawValue().typeField === 'SCAT') {

        if (this.arrayItemsScat.length > 0) {

          this.fields.forEach(field => {
            if (field.index === this.indexEdit) {
              field.label = this.addFieldForm.getRawValue().nameField,
                  field.type = this.addFieldForm.getRawValue().typeField,
                  field.required = this.stringToBool(this.addFieldForm.getRawValue().requiredField),
                  field.search = this.stringToBool(this.addFieldForm.getRawValue().searchField),
                  field.order = this.addFieldForm.getRawValue().orderField,
                  field.status = this.stringToBool(this.addFieldForm.getRawValue().statusField),
                  field.subcategories = this.arrayItemsScat
            }
          })
          this.closeModal()
        } else {
          this.toastr.warningToastr('Debe agregar items a la subcategoria', this.TITLE);
        }
      } else {

        this.fields.forEach(field => {
          if (field.index === this.indexEdit) {
            field.label = this.addFieldForm.getRawValue().nameField,
                field.type = this.addFieldForm.getRawValue().typeField,
                field.required = this.stringToBool(this.addFieldForm.getRawValue().requiredField),
                field.search = this.stringToBool(this.addFieldForm.getRawValue().searchField),
                field.order = this.addFieldForm.getRawValue().orderField,
                field.status = this.stringToBool(this.addFieldForm.getRawValue().statusField),
                field.subcategories = this.arrayItemsScat
          }
        })
        this.closeModal()
      }
    } else {
      this.toastr.warningToastr('Favor completar toda la información solicitada', this.TITLE);
    }
  }

  public deleteFieldToList(fieldRemove: IFileds) {

    this.modalRef = this.modalService.show(ConfirmComponentComponent, Object.assign( {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    }))
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.fields = this.fields.filter( function (field) { return field !== fieldRemove })
        this.closeModal()
      } else {
        this.closeModal()
      }
    })
  }

  public deleteScat(scatRemove: ISubCat) {
    this.arrayItemsScat = this.arrayItemsScat.filter( function (scat) {
      return scat !== scatRemove
    })
  }

  public deleteRep(repRemove: IManager) {
    this.addManager = this.addManager.filter( function (man) {
      return man !== repRemove
    })
  }

  public deleteNivCrit(nivRemove: ILabel) {
    this.arrayNivCrit = this.arrayNivCrit.filter( function (niv) {
      return niv !== nivRemove
    })
  }

  public addFieldList(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })

  }

  public editFieldList(template, value: IFileds) {

    this.indexEdit = value.index
    console.log(value.index)
    this.addFieldForm.controls['nameField'].setValue(value.label)
    this.addFieldForm.controls['typeField'].setValue(value.type)
    this.addFieldForm.controls['requiredField'].setValue(value.required)
    this.addFieldForm.controls['searchField'].setValue(value.search)
    this.addFieldForm.controls['orderField'].setValue(value.order)
    this.addFieldForm.controls['orderField'].setValue(value.order)
    this.addFieldForm.controls['statusField'].setValue(value.status)

    if (value.type === 'SCAT') {
      this.selectedScat = true
      this.arrayItemsScat = value.subcategories
    }
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })

  }

  public closeModal() {
    this.modalRef.hide()
    this.addFieldBuildForm()
  }

  public cancel() {
    this.router.navigate(['/app/admin/mantto-listas/list'])
  }

  public openCriticality(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public upFileList(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  private stringToBool(value): boolean {

    switch (value) {
      case 'true':
        return true
      case true:
        return true
      case 'false':
        return false
      case false:
        return false
    }
  }

  public onChangeTypeField(value) {
    if (value === 'SCAT') {
      this.selectedScat = true
    } else {
      this.selectedScat = false
    }
  }

  public validateDupField (fieldNew: IFileds) {
    let value = false
    this.fields.forEach( field => {
        if (field.label === fieldNew.label && field.type === fieldNew.type) {
          value = true
        }
    })
    return value
  }

  public guardarLista() {

  }

}
