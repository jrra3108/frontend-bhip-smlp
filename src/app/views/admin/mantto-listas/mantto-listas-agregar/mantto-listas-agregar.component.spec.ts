import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoListasAgregarComponent } from './mantto-listas-agregar.component';

describe('ManttoListasAgregarComponent', () => {
  let component: ManttoListasAgregarComponent;
  let fixture: ComponentFixture<ManttoListasAgregarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoListasAgregarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoListasAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
