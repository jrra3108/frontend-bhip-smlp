import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ManttoListasListComponent} from './mantto-listas-list/mantto-listas-list.component';
import {ManttoListasAgregarComponent} from './mantto-listas-agregar/mantto-listas-agregar.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ManttoListasListComponent,
        data: {
            title: 'Mantenimiento de listas - Listado'
        },
    },
    {
        path: 'agregar',
        component: ManttoListasAgregarComponent,
        data: {
            title: 'Mantenimiento de listas - Agregar lista'
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ManttoListasRoutingModule {
}
