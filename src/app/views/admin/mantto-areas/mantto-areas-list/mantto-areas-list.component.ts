import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {NgOption} from '@ng-select/ng-select';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
    selector: 'app-mantto-areas-list',
    templateUrl: './mantto-areas-list.component.html',
    styleUrls: ['./mantto-areas-list.component.scss']
})
export class ManttoAreasListComponent implements OnInit {

    public TITLE = 'Mantenimiento de áreas'

    public modalRef: BsModalRef

    public areaNameValue: string
    public areaIdEditValue: number
    public areaNameEditValue: string
    public areaList = []
    public managerList = []
    public respSelected: NgOption;
    public serviceOptions: NgOption[];
    public areaSelected: number
    public userCatalog = []

    constructor(
        private router: Router,
        private modalService: BsModalService,
        private apiService: ApiService,
        public toastr: ToastrManager,
    ) {
    }

    ngOnInit() {
        this.respList()
        this.apiService.getAllArea().subscribe(areas => {
            this.areaList = areas;
        })
    }

    public respList() {
        this.apiService.getUserCatalog().subscribe(data => {
            this.userCatalog = data.data
            this.serviceOptions = data.data.map(resp => {
                return {
                    id: resp.id,
                    resp: resp.usuario
                }
            })
        })
    }

    public addRepToList() {
        console.log(this.respSelected)
        if (this.respSelected !== null && this.respSelected !== undefined) {

            const body = {
                idArea: this.areaSelected,
                idResp: Number(this.respSelected.id)
            }

            this.apiService.regUserArea(body).subscribe(regUserArea => {
                console.log(regUserArea)
                if (regUserArea.code === 0) {
                    this.apiService.getUserAreaById(this.areaSelected).subscribe(userAreaList => {
                        const values = []
                        userAreaList.forEach(item => {
                            values.push({
                                id: item.id,
                                label: this.findName(item.userId)
                            })
                        })

                        this.managerList = values
                        console.log(this.managerList);
                    })
                }
            })
        } else {
            this.toastr.warningToastr('Ingrese un nombre para agregar a la lista', this.TITLE);
        }
    }

    public cancel() {
        this.router.navigate(['/app/home'])
    }

    public addRespList(template, idArea) {
        this.areaSelected = idArea;
        this.apiService.getUserAreaById(this.areaSelected).subscribe(userAreaList => {
            console.log(userAreaList)
            const values = []
            userAreaList.forEach(item => {
                values.push({
                    id: item.id,
                    label: this.findName(item.userId)
                })
            })

            this.managerList = values
            console.log(this.managerList);
        })
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public addArea(template) {
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public editArea(template, id, nombre) {
        this.areaIdEditValue = id;
        this.areaNameEditValue = nombre;
        console.log(this.areaNameEditValue)
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public execUpdate() {
        const body = {
            id: this.areaIdEditValue,
            nombre: this.areaNameEditValue
        }
        this.apiService.updateArea(body).subscribe(data => {
            console.log(data);
            this.modalRef.hide()
            this.apiService.getAllArea().subscribe(areas => {
                this.areaList = areas;
            })
        })
    }

    public removeUser(id) {
        this.apiService.removeUserArea(id).subscribe( data => {
            console.log(data);
            this.apiService.getUserAreaById(this.areaSelected).subscribe(userAreaList => {
                console.log(userAreaList)
                const values = []
                userAreaList.forEach(item => {
                    values.push({
                        id: item.id,
                        label: this.findName(item.userId)
                    })
                })

                this.managerList = values
                console.log(this.managerList);
            })
        })
    }

    public registerArea() {
        const body = {
            nombre: this.areaNameValue
        }
        this.apiService.registerArea(body).subscribe(data => {
            console.log(data)
            this.modalRef.hide()
            this.apiService.getAllArea().subscribe(areas => {
                this.areaList = areas;
            })
        })
    }

    public closeModal() {
        this.modalRef.hide()
    }

    public findName(id) {
        return this.userCatalog.filter(item => Number(item.id) === id)[0].usuario
    }

}
