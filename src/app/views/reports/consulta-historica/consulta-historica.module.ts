import {NgModule} from '@angular/core';
import {ConsultaHistoricaListComponent} from './consulta-historica-list/consulta-historica-list.component';
import {ConsultaHistoricaRoutingModule} from './consulta-historica-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


@NgModule({
    imports: [
        ConsultaHistoricaRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [ConsultaHistoricaListComponent]
})

export class ConsultaHistoricaModule {
}
