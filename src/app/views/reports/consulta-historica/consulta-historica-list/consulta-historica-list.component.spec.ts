import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaHistoricaListComponent } from './consulta-historica-list.component';

describe('ConsultaHistoricaListComponent', () => {
  let component: ConsultaHistoricaListComponent;
  let fixture: ComponentFixture<ConsultaHistoricaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaHistoricaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaHistoricaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
