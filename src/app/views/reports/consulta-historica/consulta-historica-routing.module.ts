import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConsultaHistoricaListComponent} from './consulta-historica-list/consulta-historica-list.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ConsultaHistoricaListComponent,
        data: {
            title: 'Consulta Historica de Listas - Detalle'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ConsultaHistoricaRoutingModule {
}
