import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {FormGroup} from '@angular/forms';
import {IRegisterList} from '../../../../interface/iRegisterList';
import {IList} from '../../../../interface/ilist';
import {ApiService} from '../../../../services/apiService';
import {ParseFieldsService} from '../../../../services/parseFieldsService';

@Component({
  selector: 'app-consulta-individual-detail',
  templateUrl: './consulta-individual-detail.component.html',
  styleUrls: ['./consulta-individual-detail.component.scss']
})
export class ConsultaIndividualDetailComponent implements OnInit {

  public TITLE = 'Consulta individual de Personas'
  public modalRef: BsModalRef

  public listaList: IListaCatalog[]
  public valueList: number
  public fields: FieldBase<any>[]
  public listSearchForm: FormGroup

  public fieldsRegister: IRegisterList[]
  public listaPP: IList
  public fNameAll;
  public fNacAll;
  public fFechaAll;
  public fTdocAll;
  public fDocAll;

  constructor(
      private router: Router,
      private modalService: BsModalService,
      private apiService: ApiService,
      private parse: ParseFieldsService
  ) { }

  ngOnInit() {
  }

  public createPeople() {
    this.router.navigate(['/app/upload/mantto-individual/create'])
  }
  public editPeople() {
    this.router.navigate(['/app/upload/mantto-individual/edit'])
  }

  public modalAnulation(template) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
  }

  private orderList(list) {
    return list.sort((a, b) => {
      return a.order - b.order
    })
  }

  public filterAllList() {

    const filters = [];

    if ( this.fNameAll !== undefined && this.fNameAll !== null && this.fNameAll.trim() !== '') {
      filters.push({
        index: 100201901,
        type: 'TEXT',
        value: this.fNameAll,
      })
    }

    if ( this.fNacAll !== undefined && this.fNacAll !== null && this.fNacAll.trim() !== '') {
      filters.push({
        index: 100201902,
        type: 'TEXT',
        value: this.fNacAll,
      })
    }

    if ( this.fFechaAll !== undefined && this.fFechaAll !== null && this.fFechaAll.trim() !== '') {
      filters.push({
        index: 100201903,
        type: 'DATE',
        value: this.fFechaAll,
      })
    }

    if ( this.fTdocAll !== undefined && this.fTdocAll !== null && this.fTdocAll.trim() !== '') {
      filters.push({
        index: 100201904,
        type: 'TEXT',
        value: this.fTdocAll,
      })
    }

    if ( this.fDocAll !== undefined && this.fDocAll !== null) {
      filters.push({
        index: 100201905,
        type: 'NUM',
        value: this.fDocAll,
      })
    }

    this.apiService.filterAllList(filters).subscribe( data => {
      console.log(data);
      this.fieldsRegister = data;
      this.fieldsRegister.forEach( line => {
        const regs = []
        let i = 0;
        line.registro.forEach( reg => {
          if (i < 5) {
            regs.push(reg)
          }
          i++
        })
        line.registro = regs;
        console.log(line.registro)
      })
    })
  }

}
