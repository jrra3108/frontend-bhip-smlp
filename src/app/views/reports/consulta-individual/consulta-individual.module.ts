import {NgModule} from '@angular/core';
import {ConsultaIndividualDetailComponent} from './consulta-individual-detail/consulta-individual-detail.component';
import {ConsultaIndividualRoutingModule} from './consulta-individual-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


@NgModule({
    imports: [
        ConsultaIndividualRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [ConsultaIndividualDetailComponent]
})

export class ConsultaIndividualModule {
}
