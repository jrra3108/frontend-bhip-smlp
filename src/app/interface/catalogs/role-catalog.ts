import {IResponse} from '../iResponse';

export interface IRoleCatalog {
    response: IResponse,
    data?: any
}
