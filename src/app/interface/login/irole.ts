import {IResponse} from '../iResponse';

export interface IRole {
    response: IResponse,
    profile?: any
}
