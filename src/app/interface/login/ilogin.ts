import {IResponse} from '../iResponse';

export interface ILogin {
    response: IResponse,
    token?: string
}
