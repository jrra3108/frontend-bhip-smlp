export interface IRegisterList {
    id?: number
    codLista: number
    registro: IRegisterPerson[]
    masivoIndivudual: string
    estado: boolean
    observaciones: string
    idCarga?: number
}

export interface IRegisterPerson {
    idField: any
    order: number
    value: any
}
