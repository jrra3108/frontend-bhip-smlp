export interface IProfile {
    profile_id?: number,
    profile_rol: number,
    profile_acceso: string,
    profile_visualizacion: string,
    profile_descarga: any,
    profile_responsable: any,
    fields?: IFieldsProfile[],
    profile_idLista: number,
    roles_nombre?: string,
}

export interface IFieldsProfile {
    id: number,
    label: string,
    view: boolean,
}
