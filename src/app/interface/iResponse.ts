export interface IResponse {
    code: string,
    desc: string
}
