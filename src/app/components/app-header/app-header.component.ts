import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

  constructor( private router: Router) {}

  public logout() {
    this.router.navigate(['/login'])
  }

}


